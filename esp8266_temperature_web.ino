#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WEMOS_DHT12.h>

DHT12 dht12;

const char* ssid = "SSID";
const char* password = "PSK";

ESP8266WebServer server(80);

void handleTemp() {
  if(dht12.get()==0){
    String buf;
    float tempc = dht12.cTemp;
    float humid = dht12.humidity; 
    buf += F("{\"temp\":");
    buf += String(tempc, 1);
    buf += F(",\"humidity\":");
    buf += String(humid, 1);
    buf += F("}");

    Serial.println("\n\nClient Connected");
    server.send(200, "text/plain", buf);
    Serial.println("Message Sent");
    Serial.println("Client Disconnected");
  }
}

void handleNotFound(){
  digitalWrite(LED_BUILTIN,HIGH);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(LED_BUILTIN, LOW);
 
}

void setup(void){
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/temp", handleTemp);
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}
